package com.iram.test;
import com.iram.core.Suma;

public class Tester {
	
	public void test(Suma suma)
	{
		System.out.println(testN1(suma.calcular(1)));
		System.out.println(testN4(suma.calcular(4)));
		System.out.println(testN5(suma.calcular(5)));
		System.out.println(testN9(suma.calcular(9)));
		System.out.println(testN15(suma.calcular(15)));
	}
	
	public static Boolean testN1(int result) {
		if(result == 1)
			return true;
		return false;
	}
	
	public static Boolean testN4(int result) {
		if(result == 10)
			return true;
		return false;
	}
	public static Boolean testN5(int result) {
		if(result == 15)
			return true;
		return false;
	}
	public static Boolean testN9(int result) {
		if(result == 45)
			return true;
		return false;
	}
	public static Boolean testN15(int result) {
		if(result == 120)
			return true;
		return false;
	}

}
