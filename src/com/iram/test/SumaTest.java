package com.iram.test;
import com.iram.core.Suma;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SumaTest {
	/*
	 Clase de equivalencia 1: numeros menores a 1
	 Clase de equivalencia 2: numeros mayores iguales a 1 
	 
	 Valores Limite Clase de equivalencia 1: 0,1
	 Valores Limite Clase de equivalencia 2: 0,1,2,100
	 
	 Valores de las pruebas de escritorio: 1,4,5,9,15
	 
	 */
	
	/*
	 Un accesorio de prueba(test fixture) es un conjunto de precondiciones o 
	 estados necesarios para que se ejecute una prueba. También se conoce a esto como contexto de prueba.
	 
	 Los métodos setup() y teardown() sirven para inicializar y limpiar los accesorios de prueba.
	 
	 La ejecución de una prueba de unidad individual sigue la siguiente forma:

     setup();
        Cuerpo de la prueba 
     teardown();
     
	 */
	
	Suma suma ;
	
	@BeforeEach
	void setUp() throws Exception {
		 System.out.println("Setting it up!");
		 suma = new Suma();
	}

	@AfterEach
	void tearDown() throws Exception {
		
		System.out.println("Tear it Down !");
        suma = null;
        assertNull(suma);
	}
	
/////////////////////////////////////////////////
	
	@Test
	void Suma0test() {
		assertEquals(0, suma.Calcular(0));
	}
	
	@Test
	void Suma1test() {
		assertEquals(1, suma.Calcular(1));
	}
	
	@Test
	void Suma2test() {
		assertEquals(3, suma.Calcular(2));
	}
	
	@Test
	void Suma100test() {
		assertEquals(5050, suma.Calcular(100));
	}
	
	@Test
	void Suma4test() {
		assertEquals(10, suma.Calcular(4));
	}
	
	@Test
	void Suma5test() {
		assertEquals(15, suma.Calcular(5));
	}
	
	@Test
	void Suma9test() {
		assertEquals(45, suma.Calcular(9));
	}
	
	@Test
	void Suma15test() {
		assertEquals(120, suma.Calcular(15));
	}

}

//Para correr este test, clic derecho al archivo y clic en Run as J unit Test