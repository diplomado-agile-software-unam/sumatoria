package com.iram.core;

public class Suma {
	
	public int calcular(int max) {
		 
		int suma = 0;
		
		for(int i=max;i>=1;i--)
		{
			suma = i + suma;
		}
		
		return suma;
	}
	
	public int Calcular(int num) {
		
		if(num < 0)
		{
			throw new ArithmeticException("La sumatoria no puede ser con numeros menores a 0");
		}
	    
		if (num == 0) {
			return 0;
		}
		
	    if (num == 1) {
			return 1;
		}
		
	    return num + Calcular(num -1);
	}

}
